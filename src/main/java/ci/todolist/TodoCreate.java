package ci.todolist;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@ManagedBean
public class TodoCreate {
	
	private String todoName;
	
	@Inject
    private TodoItemRepository repository;

	public String getTodoName() {
		return todoName;
	}

	public void setTodoName(String todoName) {
		this.todoName = todoName;
		repository.create(todoName);
	}
	
	

}
